package com.company.leo;

import javax.swing.*;
import java.awt.*;


public class Calculatrice extends JFrame {
    private JPanel container = new JPanel();
    private JLabel label = new JLabel();
    private Clavier clavier;

    public Calculatrice(){
        this.setSize(230, 250);
        this.setTitle("Calculette");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        container.setLayout(new FlowLayout());

        this.clavier = new Clavier();

        clavier.addObservateur(new Observateur() {
            @Override
            public void update(String value) {
                label.setText(value);
                System.out.println("val "+value);
            }
        });
        container.add(new Ecran(label),BorderLayout.NORTH);
        container.add(this.clavier,BorderLayout.EAST);

        this.getContentPane().add(container);
    }

}