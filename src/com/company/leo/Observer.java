package com.company.leo;

import javax.script.ScriptException;

interface Observable {
    public void addObservateur(Observateur obs);
    public void updateObservateur();
}