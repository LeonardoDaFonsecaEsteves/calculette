package com.company.leo;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class Ecran extends JPanel  {

    public Ecran(JLabel ecran){
        ecran.setHorizontalAlignment(JLabel.RIGHT);
        ecran.setPreferredSize(new Dimension(200, 50));;
        ecran.setBackground(Color.blue);
        Border lineBorder = BorderFactory.createLoweredBevelBorder();
        Border Title = BorderFactory.createTitledBorder(lineBorder, "Calculatrice \t ",
                TitledBorder.LEFT,TitledBorder.TOP, new Font("Arial",Font.PLAIN,8),Color.black);
        ecran.setBorder(Title);
        this.add(ecran);
    }
}
