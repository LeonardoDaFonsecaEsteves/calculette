package com.company.leo;


import javax.script.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class Clavier extends JPanel implements Observable  {
    ScriptEngineManager mgr = new ScriptEngineManager();
    ScriptEngine engine = mgr.getEngineByName("JavaScript");

    private ArrayList<Observateur> listObservateur = new ArrayList<Observateur>();
    private String[] numbre = {"1","2","3","-","4","5","6","/","7","8","9","+",".","0","=","*","CE"};
    GridLayout gl = new GridLayout();

    private String input = " ";
    private String result;

    public Clavier(){
        gl.setColumns(4);
        gl.setRows(5);
        this.setLayout(gl);
        for(int i = 0; i < numbre.length;i++){
            JButton btn = new JButton(String.valueOf(numbre[i]));
            btn.addActionListener(this::func_clic);
            this.add(btn);
        }
    }

    public void func_clic(ActionEvent e){
        if(e.getActionCommand() == "CE"){
            System.out.println("Clean");
            this.input = "0";
            this.result = null;
        }else if (e.getActionCommand() == "="){
            System.out.println("equals");
            this.getResult();
            this.input = "0";
        } else {
            this.result = null;
            if(input == "0"){
                this.input = null;
                this.input = e.getActionCommand();
            } else {
                this.input = input + e.getActionCommand();
            }
        }
        this.updateObservateur();
    }

    public  void getResult() {
        try {
            result = engine.eval(input).toString();
        } catch (ScriptException i){
            System.out.println("echec de l évaluation");
        }
        System.out.println("Result "+ result);
        this.updateObservateur();
    }

    @Override
    public void addObservateur(Observateur obs) {
        this.listObservateur.add(obs);
    }

    @Override
    public void updateObservateur() {
        for(Observateur obs : this.listObservateur )
            if(result == null){
                obs.update(input);
            } else {
                obs.update(result);
            }
    }
}
